var http = require('http');

var NODE_ENV = process.env.NODE_ENV;
var PORT = NODE_ENV === 'prod' ? 1889 : 1888;

function handleRequest(request, response) {
	response.end('NODE_ENV='+NODE_ENV+', path=' + request.url + '\n');
}
var server = http.createServer(handleRequest);
server.listen(PORT, function() {
	console.log("Server listening on: http://localhost:%s\n", PORT);
});