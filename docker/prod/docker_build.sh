#!/bin/sh

# in prod mode, source are cp to docker image
image_name="localhost:5000/my/image"
check_url="localhost:5000/v2/my/image/tags/list"
container_name="my_container"

if docker images | grep $image_name ; then
	echo "remove old image " $image_name
	docker rmi -f $image_name
else
	echo "no old image"
fi

# private docker registry https://docs.docker.com/registry/deploying/
docker build -t $image_name .

# but push to registry
docker push $image_name
echo `curl -s $check_url`