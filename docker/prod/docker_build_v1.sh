#!/bin/sh

# in prod mode, source are cp to docker image
image_name="my/image"
container_name="my_container"

# remove old image and container
if docker ps -a | grep $container_name ; then
	echo "remove old container " $container_name
	docker rm -f $container_name
else 
	echo "no old container"
fi

if docker images | grep $image_name ; then
	echo "remove old image " $image_name
	docker rmi -f $image_name
else
	echo "no old image"
fi

# sleep 3

docker build -t $image_name .
docker run -d -p 1889:1889 --name $container_name $image_name