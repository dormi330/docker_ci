#!/bin/sh

# in dev mode, source are mound to docker container
image_name="my/image_dev"
container_name="my_container_dev"

if docker images | grep $image_name ; then
	echo "image exists, do nothing"
else
	echo "no image, create new"
	docker build -t $image_name .
fi

if docker ps -a | grep $container_name ; then
	echo "restart old container"
	docker restart $container_name
else
	echo "start new container"
	docker run -d -p 1888:1888 -v $PWD:/app --name $container_name $image_name
fi

# docker run -ti -v /home/wzq/tmp/docker_ci/:/app my/image_dev
